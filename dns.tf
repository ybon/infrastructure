locals {
  dns_zone_incubateur       = "incubateur.anct.gouv.fr"
  production_cluster_cname  = "lb-kubernetes-prod.incubateur.anct.gouv.fr."
  development_cluster_cname = "lb-kubernetes-dev.incubateur.anct.gouv.fr."
}

resource "scaleway_domain_record" "grafana_dev" {
  provider = scaleway.default-project
  dns_zone = local.dns_zone_incubateur
  name     = "grafana.dev"
  type     = "CNAME"
  data     = local.development_cluster_cname
  ttl      = 3600
}

resource "scaleway_domain_record" "grafana_prod" {
  provider = scaleway.default-project
  dns_zone = local.dns_zone_incubateur
  name     = "grafana"
  type     = "CNAME"
  data     = local.production_cluster_cname
  ttl      = 3600
}

locals {
  grafana_dev_host  = "${scaleway_domain_record.grafana_dev.name}.${local.dns_zone_incubateur}"
  grafana_prod_host = "${scaleway_domain_record.grafana_prod.name}.${local.dns_zone_incubateur}"
}
