module "cluster-production" {
  source                  = "./clusters/production"
  cluster_cname           = local.production_cluster_cname
  acme_notification_email = var.acme_notification_email
  scaleway_project_id     = var.scaleway_cluster_production_project_id
  scaleway_access_key     = var.scaleway_cluster_production_access_key
  scaleway_secret_key     = var.scaleway_cluster_production_secret_key

  scaleway_default_project_id = var.scaleway_default_project_id
  scaleway_default_access_key = var.scaleway_default_access_key
  scaleway_default_secret_key = var.scaleway_default_secret_key

  decidim_host          = var.production_tools_decidim_host
  mattermost_host       = var.production_tools_mattermost_host
  matomo_host           = var.production_tools_matomo_host
  mattermost_postgresql = var.production_tools_mattermost_postgresql
  directus_host         = var.production_tools_directus_host

  outline_postgresql          = var.production_tools_outline_postgresql
  outline_clientId            = var.production_tools_outline_clientId
  outline_clientSecret        = var.production_tools_outline_clientSecret
  outline_email_smtp_host     = var.sendinblue_smtp_host
  outline_email_smtp_user     = var.production_tools_outline_email_smtp_user
  outline_email_smtp_password = var.production_tools_outline_email_smtp_password

  decidim_maires_host          = var.production_tools_decidim_maires_host
  decidim_maires_smtp_user     = var.production_tools_decidim_maires_smtp_user
  decidim_maires_smtp_password = var.production_tools_decidim_maires_smtp_password

  sendinblue_smtp_host     = var.sendinblue_smtp_host
  sendinblue_smtp_user     = var.sendinblue_smtp_user
  sendinblue_smtp_password = var.sendinblue_smtp_password
  sendinblue_smtp_port     = var.sendinblue_smtp_port
  sendinblue_smtp_secure   = var.sendinblue_smtp_secure

  grafana_host = local.grafana_prod_host

  grist_oauth_client_id     = var.production_tools_grist_oauth_client_id
  grist_oauth_client_secret = var.production_tools_grist_oauth_client_secret
  grist_default_email       = var.production_tools_grist_default_email
  grist_monitoring_org_id   = random_string.org-id-shared-donnees-territoires-transverse.result
}

module "cluster-development" {
  source                  = "./clusters/development"
  cluster_cname           = local.development_cluster_cname
  acme_notification_email = var.acme_notification_email
  scaleway_project_id     = var.scaleway_cluster_development_project_id
  scaleway_access_key     = var.scaleway_cluster_development_access_key
  scaleway_secret_key     = var.scaleway_cluster_development_secret_key

  scaleway_default_project_id = var.scaleway_default_project_id
  scaleway_default_access_key = var.scaleway_default_access_key
  scaleway_default_secret_key = var.scaleway_default_secret_key

  zammad_host = var.development_tools_zammad_host
  matomo_host = var.development_tools_matomo_host
  silab_host  = var.development_showcase_silab_host

  sendinblue_smtp_host     = var.sendinblue_smtp_host
  sendinblue_smtp_user     = var.sendinblue_smtp_user
  sendinblue_smtp_password = var.sendinblue_smtp_password
  sendinblue_smtp_port     = var.sendinblue_smtp_port
  sendinblue_smtp_secure   = var.sendinblue_smtp_secure

  grafana_host = local.grafana_dev_host
}
