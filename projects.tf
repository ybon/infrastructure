moved {
  from = module.incubator_website
  to   = module.projects.module.incubator_website
}

moved {
  from = scaleway_domain_record.incubator_website_wildcard
  to   = module.projects.scaleway_domain_record.incubator_website_wildcard
}

moved {
  from = scaleway_domain_record.incubator_website
  to   = module.projects.scaleway_domain_record.incubator_website
}

moved {
  from = module.startup_aidantsconnect
  to   = module.projects.module.startup_aidantsconnect
}

moved {
  from = scaleway_domain_record.aidantsconnect_wildcard
  to   = module.projects.scaleway_domain_record.aidantsconnect_wildcard
}

moved {
  from = scaleway_domain_record.aidantsconnect
  to   = module.projects.scaleway_domain_record.aidantsconnect
}

moved {
  from = module.startup_boursoloco
  to   = module.projects.module.startup_boursoloco
}

moved {
  from = scaleway_domain_record.boursoloco_wildcard
  to   = module.projects.scaleway_domain_record.boursoloco_wildcard
}

moved {
  from = scaleway_domain_record.boursoloco
  to   = module.projects.scaleway_domain_record.boursoloco
}

moved {
  from = module.startup_comobi
  to   = module.projects.module.startup_comobi
}

moved {
  from = scaleway_domain_record.comobi_wildcard
  to   = module.projects.scaleway_domain_record.comobi_wildcard
}

moved {
  from = scaleway_domain_record.comobi
  to   = module.projects.scaleway_domain_record.comobi
}

moved {
  from = module.startup_deveco
  to   = module.projects.module.startup_deveco
}

moved {
  from = scaleway_domain_record.deveco_wildcard
  to   = module.projects.scaleway_domain_record.deveco_wildcard
}

moved {
  from = scaleway_domain_record.deveco
  to   = module.projects.scaleway_domain_record.deveco
}

moved {
  from = module.startup_donnees_et_territoires
  to   = module.projects.module.startup_donnees_et_territoires
}

moved {
  from = scaleway_domain_record.sit
  to   = module.projects.scaleway_domain_record.sit
}

moved {
  from = scaleway_domain_record.donnees
  to   = module.projects.scaleway_domain_record.donnees
}

moved {
  from = scaleway_domain_record.donnees_wildcard
  to   = module.projects.scaleway_domain_record.donnees_wildcard
}

moved {
  from = module.startup_monsuivisocial
  to   = module.projects.module.startup_monsuivisocial
}

moved {
  from = scaleway_domain_record.monsuivisocial
  to   = module.projects.scaleway_domain_record.monsuivisocial
}

moved {
  from = scaleway_domain_record.monsuivisocial_wildcard
  to   = module.projects.scaleway_domain_record.monsuivisocial_wildcard
}

module "projects" {
  source = "./projects"
  providers = {
    scaleway = scaleway.default-project
  }
  development_cluster_cname = local.development_cluster_cname
  dns_zone_incubateur       = local.dns_zone_incubateur
  production_cluster_cname  = local.production_cluster_cname
  development_base-domain   = var.development_base-domain
  production_base-domain    = var.production_base-domain

  scaleway_organization_id                = var.scaleway_organization_id
  scaleway_cluster_development_cluster_id = module.cluster-development.scaleway_cluster_id
  scaleway_cluster_production_cluster_id  = module.cluster-production.scaleway_cluster_id


  grafana_development_auth           = module.cluster-development.grafana_auth
  grafana_development_url            = module.cluster-development.grafana_url
  grafana_development_loki_url       = module.cluster-development.loki_url
  grafana_development_prometheus_url = module.cluster-development.prometheus_url

  grafana_production_auth           = module.cluster-production.grafana_auth
  grafana_production_url            = module.cluster-production.grafana_url
  grafana_production_loki_url       = module.cluster-production.loki_url
  grafana_production_prometheus_url = module.cluster-production.prometheus_url

  sit_vm_ip             = var.sit_vm_ip
  production_haproxy_ip = module.cluster-production.haproxy_ip
}
