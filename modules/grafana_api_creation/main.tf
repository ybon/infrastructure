provider "grafana" {
  url  = var.grafana.url
  auth = var.grafana.api_key
}

resource "grafana_organization" "org" {

  name       = var.project_name
  admin_user = "terraform"
  lifecycle {
    ignore_changes = [editors, admins, viewers]
  }
}

provider "grafana" {
  alias  = "org"
  url    = var.grafana.url
  auth   = var.grafana.api_key
  org_id = grafana_organization.org.org_id
}

resource "grafana_api_key" "org" {
  provider = grafana.org

  name = "terraform"
  role = "Admin"
}
