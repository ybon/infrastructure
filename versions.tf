terraform {
  required_providers {
    scaleway = {
      source  = "scaleway/scaleway"
      version = "~> 2.2.0"
    }
    grafana = {
      source  = "grafana/grafana"
      version = "~> 1.28.2"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 3.15.1"
    }
    random = {
      source  = "hashicorp/random"
      version = "~> 3.3.0"
    }
  }
}
