terraform {
  required_version = ">= 1.0"
  backend "http" {
    retry_wait_min = 5
    lock_method    = "POST"
    unlock_method  = "DELETE"
  }
}
