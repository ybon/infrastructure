resource "scaleway_domain_record" "incubator_website" {
  dns_zone = var.dns_zone_incubateur
  name     = "dev"
  type     = "CNAME"
  data     = var.development_cluster_cname
  ttl      = 3600
}

resource "scaleway_domain_record" "incubator_website_wildcard" {
  dns_zone = var.dns_zone_incubateur
  name     = "*.dev"
  type     = "CNAME"
  data     = var.development_cluster_cname
  ttl      = 3600
}

moved {
  from = module.grafana_production
  to   = module.incubator_website_grafana_production
}

module "incubator_website_grafana_production" {
  source = "../modules/grafana_api_creation"
  grafana = {
    url     = var.grafana_production_url
    api_key = var.grafana_production_auth
  }
  project_name = "Site web incubateur"
}

moved {
  from = module.grafana_development
  to   = module.incubator_website_grafana_development
}

module "incubator_website_grafana_development" {
  source = "../modules/grafana_api_creation"
  grafana = {
    url     = var.grafana_development_url
    api_key = var.grafana_development_auth
  }
  project_name = "Site web incubateur"
}

module "incubator_website" {
  source  = "gitlab.com/vigigloo/tf-modules/gitlabterraformproject"
  version = "0.4.5"

  gitlab_project_location = data.gitlab_group.projects.group_id
  project_name            = "Site web incubateur"
  project_slug            = "incubator_website"

  dev_base-domain  = var.development_base-domain
  prod_base-domain = var.production_base-domain

  no_gitlab_tokens = true

  scaleway_organization_id                = var.scaleway_organization_id
  scaleway_cluster_development_cluster_id = var.scaleway_cluster_development_cluster_id
  scaleway_cluster_production_cluster_id  = var.scaleway_cluster_production_cluster_id

  grafana_development = {
    url            = var.grafana_development_url
    api_key        = module.incubator_website_grafana_development.api_key
    org_id         = module.incubator_website_grafana_development.org_id
    loki_url       = var.grafana_development_loki_url
    prometheus_url = var.grafana_development_prometheus_url
  }
  grafana_production = {
    url            = var.grafana_production_url
    api_key        = module.incubator_website_grafana_production.api_key
    org_id         = module.incubator_website_grafana_production.org_id
    loki_url       = var.grafana_production_loki_url
    prometheus_url = var.grafana_production_prometheus_url
  }
}

output "incubator_website_var_file" {
  value = module.incubator_website.var_file
}
