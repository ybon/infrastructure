data "gitlab_group" "projects" {
  group_id = 15577471
}

variable "dns_zone_incubateur" {
  type = string
}
variable "production_cluster_cname" {
  type = string
}
variable "development_cluster_cname" {
  type = string
}

variable "development_base-domain" {
  type = string
}
variable "production_base-domain" {
  type = string
}
variable "scaleway_organization_id" {
  type = string
}
variable "scaleway_cluster_development_cluster_id" {
  type = string
}
variable "scaleway_cluster_production_cluster_id" {
  type = string
}

variable "grafana_production_url" {
  type = string
}
variable "grafana_production_auth" {
  type = string
}
variable "grafana_production_loki_url" {
  type = string
}
variable "grafana_production_prometheus_url" {
  type = string
}

variable "grafana_development_url" {
  type = string
}
variable "grafana_development_auth" {
  type = string
}
variable "grafana_development_loki_url" {
  type = string
}
variable "grafana_development_prometheus_url" {
  type = string
}

variable "sit_vm_ip" {
  type = string
}

variable "production_haproxy_ip" {
  type = string
}
