resource "scaleway_domain_record" "monsuivisocial" {
  dns_zone = var.dns_zone_incubateur
  name     = "monsuivisocial"
  type     = "CNAME"
  data     = var.production_cluster_cname
  ttl      = 3600
}

resource "scaleway_domain_record" "monsuivisocial_wildcard" {
  dns_zone = var.dns_zone_incubateur
  name     = "*.monsuivisocial"
  type     = "CNAME"
  data     = var.production_cluster_cname
  ttl      = 3600
}

module "monsuivisocial_grafana_production" {
  source = "../modules/grafana_api_creation"
  grafana = {
    url     = var.grafana_production_url
    api_key = var.grafana_production_auth
  }
  project_name = "Monsuivisocial"
}

module "monsuivisocial_grafana_development" {
  source = "../modules/grafana_api_creation"
  grafana = {
    url     = var.grafana_development_url
    api_key = var.grafana_development_auth
  }
  project_name = "Monsuivisocial"
}

module "startup_monsuivisocial" {
  source  = "gitlab.com/vigigloo/tf-modules/gitlabterraformproject"
  version = "0.4.5"

  gitlab_project_location = data.gitlab_group.projects.group_id
  project_name            = "Monsuivisocial"
  project_slug            = "monsuivisocial"

  dev_base-domain  = "monsuivisocial.${var.development_base-domain}"
  prod_base-domain = "monsuivisocial.${var.production_base-domain}"

  no_gitlab_tokens = true

  scaleway_organization_id                = var.scaleway_organization_id
  scaleway_cluster_development_cluster_id = var.scaleway_cluster_development_cluster_id
  scaleway_cluster_production_cluster_id  = var.scaleway_cluster_production_cluster_id

  grafana_development = {
    url            = var.grafana_development_url
    api_key        = module.monsuivisocial_grafana_development.api_key
    org_id         = module.monsuivisocial_grafana_development.org_id
    loki_url       = var.grafana_development_loki_url
    prometheus_url = var.grafana_development_prometheus_url
  }
  grafana_production = {
    url            = var.grafana_production_url
    api_key        = module.monsuivisocial_grafana_production.api_key
    org_id         = module.monsuivisocial_grafana_production.org_id
    loki_url       = var.grafana_production_loki_url
    prometheus_url = var.grafana_production_prometheus_url
  }
}

output "monsuivisocial_var_file" {
  value = module.startup_monsuivisocial.var_file
}

