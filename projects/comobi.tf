resource "scaleway_domain_record" "comobi" {
  dns_zone = var.dns_zone_incubateur
  name     = "comobi"
  type     = "CNAME"
  data     = var.production_cluster_cname
  ttl      = 3600
}

resource "scaleway_domain_record" "comobi_wildcard" {
  dns_zone = var.dns_zone_incubateur
  name     = "*.comobi"
  type     = "CNAME"
  data     = var.production_cluster_cname
  ttl      = 3600
}


module "comobi_grafana_production" {
  source = "../modules/grafana_api_creation"
  grafana = {
    url     = var.grafana_production_url
    api_key = var.grafana_production_auth
  }
  project_name = "Comobi"
}

module "comobi_grafana_development" {
  source = "../modules/grafana_api_creation"
  grafana = {
    url     = var.grafana_development_url
    api_key = var.grafana_development_auth
  }
  project_name = "Comobi"
}

module "startup_comobi" {
  source  = "gitlab.com/vigigloo/tf-modules/gitlabterraformproject"
  version = "0.4.5"

  gitlab_project_location = data.gitlab_group.projects.group_id
  project_name            = "Comobi"
  project_slug            = "comobi"

  dev_base-domain  = "comobi.${var.development_base-domain}"
  prod_base-domain = "comobi.${var.production_base-domain}"

  no_gitlab_tokens = true

  scaleway_organization_id                = var.scaleway_organization_id
  scaleway_cluster_development_cluster_id = var.scaleway_cluster_development_cluster_id
  scaleway_cluster_production_cluster_id  = var.scaleway_cluster_production_cluster_id

  grafana_development = {
    url            = var.grafana_development_url
    api_key        = module.comobi_grafana_development.api_key
    org_id         = module.comobi_grafana_development.org_id
    loki_url       = var.grafana_development_loki_url
    prometheus_url = var.grafana_development_prometheus_url
  }
  grafana_production = {
    url            = var.grafana_production_url
    api_key        = module.comobi_grafana_production.api_key
    org_id         = module.comobi_grafana_production.org_id
    loki_url       = var.grafana_production_loki_url
    prometheus_url = var.grafana_production_prometheus_url
  }
}

output "comobi_var_file" {
  value = module.startup_comobi.var_file
}
