module "devops-gitlab-runner" {
  source  = "gitlab.com/vigigloo/tf-modules/k8sgitlabrunner"
  version = "0.2.2"

  kubeconfig   = scaleway_k8s_cluster.development.kubeconfig[0]
  project_slug = "devops-infra"
  project_name = "devOpsInfra"

  chart_version = "0.41.0"

  gitlab_groups = [
    13501540, // https://gitlab.com/incubateur-territoires/devops
    52940215, // https://gitlab.com/incubateur-territoires/ateliers
    15978123, // https://gitlab.com/incubateur-territoires/france-relance
    13506047, // https://gitlab.com/incubateur-territoires/incubateur
  ]
}

module "global-docker-runner" {
  source  = "gitlab.com/vigigloo/tf-modules/gitlabinstancerunner"
  version = "0.0.8"

  privileged            = true
  enable_ipv4           = true
  enable_ipv6           = true
  gitlab_runner-version = "15.0.0"
  gitlab_tags           = ["dind", "scaleway", "alpine"]

  gitlab_groups = [
    13501534, //https://gitlab.com/incubateur-territoires
  ]
}
