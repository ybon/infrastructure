module "zammad" {
  source      = "./tools/zammad"
  zammad_host = var.zammad_host
}

module "matomo" {
  source        = "./tools/matomo"
  cluster_cname = var.cluster_cname
  matomo_host   = var.matomo_host
}
