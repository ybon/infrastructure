output "scaleway_cluster_id" {
  value = scaleway_k8s_cluster.development.id
}

output "haproxy_ip" {
  value = scaleway_lb_ip.haproxy-ip.ip_address
}

output "grafana_auth" {
  value     = "${module.grafana.grafana_admin_user}:${module.grafana.grafana_admin_password}"
  sensitive = true
}

output "grafana_url" {
  value = module.grafana.url
}

output "loki_url" {
  value = module.loki.service_url
}

output "prometheus_url" {
  value = module.mimir.internal_prometheus_url
}
