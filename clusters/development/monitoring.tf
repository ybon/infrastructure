resource "kubernetes_namespace" "monitoring" {
  metadata {
    name = "monitoring"
  }
}

resource "scaleway_object_bucket" "logs" {
  name = "anct-incubateur-logs-dev"
}

module "loki" {
  source     = "gitlab.com/vigigloo/tools-k8s-grafana/loki"
  version    = "0.0.3"
  chart_name = "loki"
  namespace  = kubernetes_namespace.monitoring.metadata[0].name

  values = [
    <<-EOT
    global:
      dnsService: coredns
    compactor:
      enabled: true
    loki:
      structuredConfig:
        auth_enabled: true
        compactor:
          shared_store: aws
          retention_enabled: true
          retention_delete_delay: 360h
      schemaConfig:
        configs:
          - from: 2022-08-01
            store: boltdb-shipper
            object_store: aws
            schema: v11
            index:
              prefix: loki_index_
              period: 24h
      storageConfig:
        boltdb_shipper:
          shared_store: aws
        aws:
          endpoint: https://s3.${scaleway_object_bucket.logs.region}.scw.cloud
          access_key_id: ${var.scaleway_access_key}
          secret_access_key: ${var.scaleway_secret_key}
          region: ${scaleway_object_bucket.logs.region}
          bucketnames: ${scaleway_object_bucket.logs.name}
    EOT
  ]
}

module "grafana" {
  source                    = "gitlab.com/vigigloo/tools-k8s-grafana/grafana"
  version                   = "0.0.3"
  chart_name                = "grafana"
  namespace                 = kubernetes_namespace.monitoring.metadata[0].name
  grafana_admin_secret_name = "grafana-terraform-user"
  grafana_admin_user        = "terraform"

  values = [
    <<-EOT
    image:
      tag: 9.1.0
    grafana.ini:
      server:
        root_url: https://${var.grafana_host}/
      smtp:
        enabled: true
        host: ${var.sendinblue_smtp_host}:${var.sendinblue_smtp_port}
        user: ${var.sendinblue_smtp_user}
        password: ${var.sendinblue_smtp_password}
        from_address: ${var.sendinblue_smtp_user}
    persistence:
      enabled: true
    ingress:
      enabled: true
      annotations:
        acme.cert-manager.io/http01-edit-in-place: "true"
        cert-manager.io/cluster-issuer: letsencrypt-prod
        kubernetes.io/ingress.class: haproxy
      path: /

      hosts:
        - ${var.grafana_host}

      tls:
        - secretName: grafana-tls
          hosts:
            - ${var.grafana_host}
    EOT
  ]
}

module "promtail" {
  source     = "gitlab.com/vigigloo/tools-k8s-grafana/promtail"
  version    = "0.0.1"
  chart_name = "promtail"
  namespace  = kubernetes_namespace.monitoring.metadata[0].name
  depends_on = [module.loki]

  values = [
    <<-EOT
    config:
      snippets:
        extraRelabelConfigs:
          - action: replace
            source_labels:
              - __meta_kubernetes_pod_annotation_monitoring_org_id
            target_label: _monitoring_org_id
        pipelineStages:
          - cri: {}
          - json:
              expressions:
                output: log
                stream: stream
                timestamp: time
          - tenant:
              label: _monitoring_org_id
          - labeldrop:
              - _monitoring_org_id
      clients:
        - url: http://${module.loki.internal_gateway}/loki/api/v1/push
    EOT
  ]
}

resource "scaleway_object_bucket" "mimir_alertmanager_storage" {
  name = "anct-incubateur-mimir-alertmanager-storage-dev"
}
resource "scaleway_object_bucket" "mimir_blocks_storage" {
  name = "anct-incubateur-mimir-blocks-storage-dev"
}
resource "scaleway_object_bucket" "mimir_ruler_storage" {
  name = "anct-incubateur-mimir-ruler-storage-dev"
}
module "mimir" {
  source     = "gitlab.com/vigigloo/tools-k8s-grafana/mimir"
  version    = "0.0.3"
  chart_name = "mimir"
  namespace  = kubernetes_namespace.monitoring.metadata[0].name

  values = [
    <<-EOT
    global:
      dnsService: coredns
    minio:
      enabled: false
    mimir:
      structuredConfig:
        alertmanager_storage:
          backend: s3
          s3:
            access_key_id: ${var.scaleway_access_key}
            bucket_name: ${scaleway_object_bucket.mimir_alertmanager_storage.name}
            region: ${scaleway_object_bucket.mimir_alertmanager_storage.region}
            endpoint: s3.${scaleway_object_bucket.mimir_alertmanager_storage.region}.scw.cloud
            secret_access_key: ${var.scaleway_secret_key}
        blocks_storage:
          backend: s3
          s3:
            access_key_id: ${var.scaleway_access_key}
            bucket_name: ${scaleway_object_bucket.mimir_blocks_storage.name}
            region: ${scaleway_object_bucket.mimir_blocks_storage.region}
            endpoint: s3.${scaleway_object_bucket.mimir_blocks_storage.region}.scw.cloud
            secret_access_key: ${var.scaleway_secret_key}
        ruler_storage:
          backend: s3
          s3:
            access_key_id: ${var.scaleway_access_key}
            bucket_name: ${scaleway_object_bucket.mimir_ruler_storage.name}
            region: ${scaleway_object_bucket.mimir_ruler_storage.region}
            endpoint: s3.${scaleway_object_bucket.mimir_ruler_storage.region}.scw.cloud
            secret_access_key: ${var.scaleway_secret_key}
    EOT
  ]
}

module "prometheus" {
  source     = "gitlab.com/vigigloo/tools-k8s/prometheus"
  version    = "0.0.1"
  chart_name = "prometheus"
  namespace  = kubernetes_namespace.monitoring.metadata[0].name
  values = [
    <<-EOT
    server:
      remoteWrite:
        - url: ${module.cortex_tenant.internal_url}/push
    extraScrapeConfigs: |
      - job_name: 'label_org_id'
        relabel_configs:
          - source_labels: [__meta_kubernetes_pod_annotation_monitoring_org_id]
            action: replace
            target_label: monitoring_org_id
    EOT
  ]
}

module "cortex_tenant" {
  source                     = "gitlab.com/vigigloo/tools-k8s-cortex/tenant"
  version                    = "0.0.2"
  chart_name                 = "cortex-tenant"
  namespace                  = kubernetes_namespace.monitoring.metadata[0].name
  cortex_tenant_label        = "label_org_id"
  cortex_tenant_label_remove = true
  cortex_tenant_target       = module.mimir.internal_push_url
  cortex_tenant_default      = "default"
}
