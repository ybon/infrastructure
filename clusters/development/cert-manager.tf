resource "kubernetes_namespace" "cert-manager" {
  metadata {
    name = "cert-manager"
  }
}

resource "helm_release" "cert-manager" {
  depends_on = [scaleway_k8s_pool.default]
  name       = "cert-manager"
  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  version    = "v1.6.1"
  wait       = true
  namespace  = kubernetes_namespace.cert-manager.metadata[0].name
  set {
    name  = "installCRDs"
    value = true
  }
}

resource "kubernetes_manifest" "cert-manager-clusterissuer-letsencrypt-staging" {
  depends_on = [helm_release.cert-manager]

  manifest = {
    "apiVersion" = "cert-manager.io/v1"
    "kind"       = "ClusterIssuer"
    "metadata" = {
      "name" = "letsencrypt-staging"
    }
    "spec" = {
      "acme" = {
        "privateKeySecretRef" = {
          "name" = "letsencrypt-staging"
        }
        "server" = "https://acme-staging-v02.api.letsencrypt.org/directory"
        "solvers" = [
          {
            "http01" = {
              "ingress" = {
                "class" = "haproxy"
              }
            }
            "selector" = {}
          },
        ]
      }
    }
  }
}

resource "kubernetes_manifest" "cert-manager-clusterissuer-letsencrypt-prod" {
  depends_on = [helm_release.cert-manager]

  manifest = {
    "apiVersion" = "cert-manager.io/v1"
    "kind"       = "ClusterIssuer"
    "metadata" = {
      "name" = "letsencrypt-prod"
    }
    "spec" = {
      "acme" = {
        "privateKeySecretRef" = {
          "name" = "letsencrypt-prod"
        }
        "server" = "https://acme-v02.api.letsencrypt.org/directory"
        "solvers" = [
          {
            "http01" = {
              "ingress" = {
                "class" = "haproxy"
              }
            }
            "selector" = {}
          },
        ]
      }
    }
  }
}
