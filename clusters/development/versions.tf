terraform {
  required_providers {
    kubernetes = {
      version = "~> 2.13.1"
      source  = "hashicorp/kubernetes"
    }
    helm = {
      version = "~> 2.5.0"
      source  = "hashicorp/helm"
    }
    scaleway = {
      source = "scaleway/scaleway"
    }
  }
}
