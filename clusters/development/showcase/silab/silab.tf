module "namespace" {
  source       = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version      = "0.1.0"
  max_cpu      = 4
  max_memory   = "4Gi"
  namespace    = "silab"
  project_name = "SIlab"
  project_slug = "silab"
}

resource "helm_release" "silab" {
  name       = "silab"
  repository = "https://gitlab.com/api/v4/projects/38105061/packages/helm/stable"
  chart      = "silab"
  namespace  = module.namespace.namespace
  values = [
    file("${path.module}/silab.yaml"),
    <<-EOT
    ingress:
      host: ${var.silab_host}
    EOT
  ]
}
