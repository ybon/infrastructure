resource "kubernetes_namespace" "gateway" {
  metadata {
    name = "gateway"
  }
}

resource "scaleway_lb_ip" "haproxy-ip" {}

module "haproxy" {
  source        = "gitlab.com/vigigloo/tools-k8s/haproxy"
  version       = "0.1.1"
  namespace     = kubernetes_namespace.gateway.metadata[0].name
  chart_name    = "haproxy"
  haproxy_lb_ip = scaleway_lb_ip.haproxy-ip.ip_address
  haproxy_config_response_set_header = [
    {
      name  = "X-Frame-Options"
      value = "SAMEORIGIN"
    },
    {
      name  = "Referrer-Policy"
      value = "no-referrer"
    },
    {
      name  = "Strict-Transport-Security"
      value = "max-age=31536000"
    }
  ]
}
