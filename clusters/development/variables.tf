variable "cluster_cname" {
  type = string
}

variable "acme_notification_email" {
  type = string
}

variable "scaleway_project_id" {
  type = string
}

variable "scaleway_access_key" {
  type      = string
  sensitive = true
}

variable "scaleway_secret_key" {
  type      = string
  sensitive = true
}

variable "scaleway_default_project_id" {
  type = string
}

variable "scaleway_default_access_key" {
  type      = string
  sensitive = true
}

variable "scaleway_default_secret_key" {
  type      = string
  sensitive = true
}

variable "zammad_host" {
  type = string
}

variable "matomo_host" {
  type = string
}

variable "silab_host" {
  type = string
}

variable "grafana_host" {
  type = string
}


variable "sendinblue_smtp_host" {
  type = string
}
variable "sendinblue_smtp_user" {
  type = string
}
variable "sendinblue_smtp_password" {
  type      = string
  sensitive = true
}
variable "sendinblue_smtp_port" {
  type = string
}
variable "sendinblue_smtp_secure" {
  type = string
}
