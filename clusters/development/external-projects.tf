module "forum" {
  source              = "./external-projects/forum"
  scaleway_access_key = var.scaleway_default_access_key
  scaleway_secret_key = var.scaleway_default_secret_key
  scaleway_project_id = var.scaleway_default_project_id
}
