resource "scaleway_k8s_cluster" "development" {
  name        = "Incubateur-development"
  description = "Managed by terraform"
  version     = "1.24.3"
  cni         = "calico"
  tags        = ["development", "terraform"]
  project_id  = var.scaleway_project_id
  lifecycle {
    prevent_destroy = true
  }
}

resource "scaleway_k8s_pool" "default" {
  cluster_id          = scaleway_k8s_cluster.development.id
  name                = "default"
  node_type           = "GP1-S"
  size                = 1
  autohealing         = true
  wait_for_pool_ready = true
  autoscaling         = true
  max_size            = 5
  lifecycle {
    prevent_destroy = true
  }
}
