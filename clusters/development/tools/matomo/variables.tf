variable "cluster_cname" {
  type = string
}

variable "matomo_host" {
  type = string
}
