# For live examples please refer to others module in this folder

resource "scaleway_domain_record" "<module_name>" {
  dns_zone = "incubateur.anct.gouv.fr"
  name     = "<subdomain_name>"
  type     = "<record_type>"
  data     = "<record_ip>"
  ttl      = 3600
}
