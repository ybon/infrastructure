locals {
  forum_vm_ip = "51.159.157.165"
}
resource "scaleway_domain_record" "forum" {
  dns_zone = "incubateur.anct.gouv.fr"
  name     = "forum"
  type     = "A"
  data     = local.forum_vm_ip
  ttl      = 3600
}
