resource "scaleway_domain_record" "dotations_locales" {
  dns_zone = "incubateur.anct.gouv.fr"
  name     = "dotations"
  type     = "CNAME"
  data     = "dotations-locales-app.osc-fr1.scalingo.io."
  ttl      = 3600
}
