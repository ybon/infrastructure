resource "scaleway_domain_record" "store" {
  dns_zone = "territoires.store"
  name     = ""
  type     = "A"
  data     = var.loadbalancer_ip
  ttl      = 3600
}

resource "scaleway_domain_record" "www" {
  dns_zone = scaleway_domain_record.store.dns_zone
  name     = "www"
  type     = "CNAME"
  data     = "${scaleway_domain_record.store.dns_zone}."
  ttl      = 3600
}

resource "scaleway_domain_record" "api" {
  dns_zone = scaleway_domain_record.store.dns_zone
  name     = "api"
  type     = "CNAME"
  data     = "${scaleway_domain_record.store.dns_zone}."
  ttl      = 3600
}

module "namespace" {
  source       = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version      = "0.1.0"
  max_cpu      = 2
  max_memory   = "2Gi"
  namespace    = "redirection"
  project_name = "Redirection"
  project_slug = "redirection"
}

module "redirection" {
  source  = "gitlab.com/vigigloo/tools-k8s/nginxredirect"
  version = "0.1.0"

  chart_name    = "redirection"
  chart_version = "0.1.0"
  namespace     = module.namespace.namespace

  values = [
    file("${path.module}/redirect.yaml"),
    <<-EOT
    ingress:
      className: null
      enabled: true
      certManagerClusterIssuer: letsencrypt-prod
      host: null
      annotations:
        acme.cert-manager.io/http01-edit-in-place: "true"
      hosts:
         - host: www.${scaleway_domain_record.store.dns_zone}
           paths:
             - path: /
               pathType: Prefix
         - host: api.${scaleway_domain_record.store.dns_zone}
           paths:
             - path: /
               pathType: Prefix
      tls:
        enabled: true
        additional:
         - secretName: www-${replace(scaleway_domain_record.store.dns_zone, ".", "-")}-tls
           hosts:
             - www.${scaleway_domain_record.store.dns_zone}
         - secretName: api-${replace(scaleway_domain_record.store.dns_zone, ".", "-")}-tls
           hosts:
             - api.${scaleway_domain_record.store.dns_zone}
    EOT
  ]
  redirect_from = scaleway_domain_record.store.dns_zone
  redirect_to   = "https://incubateur.anct.gouv.fr"

  requests_cpu    = "10m"
  requests_memory = "20Mi"
}
