module "mattermost-namespace" {
  source       = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version      = "0.1.0"
  max_cpu      = 6
  max_memory   = "8Gi"
  namespace    = "mattermost"
  project_name = "Mattermost"
  project_slug = "mattermost"
}

module "mattermost" {
  source  = "gitlab.com/vigigloo/tools-k8s/mattermost"
  version = "0.1.0"

  chart_name    = "mattermost"
  chart_version = "6.3.1"
  namespace     = module.mattermost-namespace.namespace

  image_repository = "mattermost/mattermost-team-edition"
  image_tag        = "7.0"
  ingress_host     = var.mattermost_host
  values = [
    file("${path.module}/mattermost.yaml")
  ]
  mattermost_externaldb                         = "postgres"
  mattermost_externaldb_connection_string       = var.mattermost_postgresql
  mattermost_configJSON_ServiceSettings_SiteURL = "https://${var.mattermost_host}"

  limits_cpu      = "2000m"
  limits_memory   = "2048Mi"
  requests_cpu    = "1000m"
  requests_memory = "1024Mi"
}
