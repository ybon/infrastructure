resource "scaleway_domain_record" "chat" {
  dns_zone = "incubateur.anct.gouv.fr"
  name     = "chat"
  type     = "CNAME"
  data     = var.cluster_cname
  ttl      = 3600
}
