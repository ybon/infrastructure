resource "scaleway_domain_record" "mattermost" {
  dns_zone = "swy.territoires.fyi"
  name     = "mattermost"
  type     = "CNAME"
  data     = var.cluster_cname
  ttl      = 3600
}

module "redirection" {
  source  = "gitlab.com/vigigloo/tools-k8s/nginxredirect"
  version = "0.1.0"

  chart_name    = "redirection"
  chart_version = "0.1.0"
  namespace     = module.mattermost-namespace.namespace

  values = [
    file("${path.module}/redirect.yaml")
  ]
  redirect_from = "${scaleway_domain_record.mattermost.name}.${scaleway_domain_record.mattermost.dns_zone}"
  redirect_to   = "https://${scaleway_domain_record.chat.name}.${scaleway_domain_record.chat.dns_zone}"

  requests_cpu    = "10m"
  requests_memory = "20Mi"
}
