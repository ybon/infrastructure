variable "scaleway_access_key" {
  type = string
}

variable "scaleway_secret_key" {
  type = string
}

variable "scaleway_project_id" {
  type = string
}

variable "mattermost_host" {
  type = string
}

variable "mattermost_postgresql" {
  type = string
}

variable "cluster_cname" {
  type = string
}
