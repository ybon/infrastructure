variable "scaleway_default_access_key" {
  type = string
}

variable "scaleway_default_secret_key" {
  type = string
}

variable "scaleway_default_project_id" {
  type = string
}

variable "scaleway_access_key" {
  type = string
}

variable "scaleway_secret_key" {
  type = string
}

variable "scaleway_project_id" {
  type = string
}

variable "decidim_host" {
  type = string
}

variable "decidim_smtp_user" {
  type = string
}
variable "decidim_smtp_password" {
  type = string
}

variable "cluster_cname" {
  type = string
}

variable "kubeconfig" {
  type = object({
    host                   = string
    token                  = string
    cluster_ca_certificate = string
  })
}
