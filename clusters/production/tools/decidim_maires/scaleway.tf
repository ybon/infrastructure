resource "scaleway_object_bucket" "decidim" {
  name   = "anct-decidim-maires"
  acl    = "public-read"
  region = "fr-par"
}

resource "scaleway_object_bucket" "decidim_backups" {
  name   = "anct-decidim-maires-database-backups"
  region = "fr-par"
}
