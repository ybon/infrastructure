module "kubeconfig_vincent" {
  source  = "gitlab.com/vigigloo/tf-modules/generatescopedkubeconfig"
  version = "0.1.0"

  filename               = "decidim-maires-vincent.yml"
  namespace              = module.namespace.namespace
  username               = "vincent"
  project_slug           = local.project_slug
  project_name           = local.project_name
  role_name              = module.namespace.role_name
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = var.kubeconfig.cluster_ca_certificate
}
