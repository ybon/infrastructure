locals {
  project_name = "DecidimMaires"
  project_slug = "decidim-maires"
}

module "namespace" {
  source       = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version      = "0.1.0"
  max_cpu      = 9
  max_memory   = "20Gi"
  namespace    = "decidim-maires"
  project_slug = local.project_slug
  project_name = local.project_name
}

resource "random_password" "decidim_secret_key" {
  length  = 128
  special = false
}

module "postgresql" {
  source     = "gitlab.com/vigigloo/tools-k8s-crunchydata/pgcluster"
  version    = "0.0.14"
  namespace  = module.namespace.namespace
  chart_name = "postgresql"

  values = [
    templatefile("${path.module}/backups.yaml", {
      bucket_name         = resource.scaleway_object_bucket.decidim_backups.name
      scaleway_access_key = var.scaleway_access_key
      scaleway_secret_key = var.scaleway_secret_key
      backup_offset_min   = "5"
    })
  ]
}

module "decidim" {
  source           = "gitlab.com/vigigloo/tools-k8s/decidim"
  version          = "0.0.5"
  namespace        = module.namespace.namespace
  chart_name       = "decidim"
  chart_version    = "0.1.5"
  image_repository = "registry.gitlab.com/incubateur-territoires/incubateur/decidim-ditp"
  image_tag        = "0.1.1"
  values           = [file("${path.module}/decidim.yaml")]
  ingress_host     = var.decidim_host

  decidim_secret_key_base = random_password.decidim_secret_key.result

  decidim_postgresql_database = module.postgresql.dbname
  decidim_postgresql_host     = module.postgresql.host
  decidim_postgresql_username = module.postgresql.user
  decidim_postgresql_password = replace(module.postgresql.password, ",", "\\,")
  decidim_postgresql_port     = module.postgresql.port

  decidim_s3_bucket            = scaleway_object_bucket.decidim.name
  decidim_s3_host              = "${scaleway_object_bucket.decidim.name}.s3.${scaleway_object_bucket.decidim.region}.scw.cloud"
  decidim_s3_region            = scaleway_object_bucket.decidim.region
  decidim_s3_access_key_id     = var.scaleway_access_key
  decidim_s3_secret_access_key = var.scaleway_secret_key

  decidim_redis_enabled = true
  decidim_redis_url     = "redis://${module.redis.hostname}"

  decidim_email_smtp_host     = "smtp-relay.sendinblue.com"
  decidim_email_smtp_password = var.decidim_smtp_password
  decidim_email_smtp_user     = var.decidim_smtp_user
  decidim_email_smtp_secure   = false
  decidim_email_smtp_port     = 587

  requests_cpu    = "10m"
  requests_memory = "1200Mi"
  limits_memory   = "2Gi"
}

module "redis" {
  source         = "gitlab.com/vigigloo/tools-k8s/redis"
  namespace      = module.namespace.namespace
  chart_name     = "redis"
  redis_password = "redis"
  redis_replicas = 0
  values         = [file("${path.module}/redis.yaml")]

  requests_cpu    = "20m"
  requests_memory = "40Mi"
}
