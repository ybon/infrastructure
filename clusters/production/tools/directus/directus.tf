module "namespace" {
  source       = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version      = "0.1.0"
  max_cpu      = 9
  max_memory   = "20Gi"
  namespace    = "directus"
  project_name = "Directus"
  project_slug = "directus"
}

resource "random_string" "directus-secret" {
  length  = 128
  special = false
  upper   = false
  lifecycle {
    ignore_changes = all
  }
}

resource "random_string" "directus-key" {
  length  = 128
  special = false
  upper   = false
  lifecycle {
    ignore_changes = all
  }
}

//Hack to store port
resource "random_string" "directus_database_port" {
  length = 5
  lifecycle {
    ignore_changes = all
  }
}

//Hack to store ip
resource "random_string" "directus_database_host" {
  length = 13
  lifecycle {
    ignore_changes = all
  }
}

resource "scaleway_object_bucket" "directus" {
  provider = scaleway.default-project
  name     = "incubateur-directus"
  acl      = "public-read"
  region   = "fr-par"
  lifecycle {
    ignore_changes = all
  }
}

data "scaleway_rdb_instance" "directus" {
  provider    = scaleway.default-project
  instance_id = "7841c630-c9ff-44cd-9917-1085aa43d080"
}

data "scaleway_rdb_database" "directus" {
  provider    = scaleway.default-project
  instance_id = data.scaleway_rdb_instance.directus.instance_id
  name        = "directus"
}

resource "random_password" "directus-database-password" {
  length = 20
  lifecycle {
    ignore_changes = all
  }
}
module "directus" {
  source        = "gitlab.com/vigigloo/tools-k8s/directus"
  version       = "0.2.1"
  chart_name    = "directus"
  chart_version = "0.3.1"
  image_tag     = "9.7.1"
  namespace     = module.namespace.namespace
  ingress_host  = var.directus_host
  values = [
    file("${path.module}/directus.yaml")
  ]
  directus_secret     = random_string.directus-secret.result
  directus_public_url = "https://${var.directus_host}"
  directus_key        = random_string.directus-key.result

  directus_postgresql_host     = random_string.directus_database_host.result
  directus_postgresql_database = data.scaleway_rdb_database.directus.name
  directus_postgresql_username = data.scaleway_rdb_database.directus.name
  directus_postgresql_password = random_password.directus-database-password.result
  directus_postgresql_port     = random_string.directus_database_port.result

  directus_email_from          = "directus@incubateur.anct.gouv.fr"
  directus_email_smtp_host     = var.sendinblue_smtp_host
  directus_email_smtp_user     = var.sendinblue_smtp_user
  directus_email_smtp_password = var.sendinblue_smtp_password
  directus_email_smtp_port     = var.sendinblue_smtp_port
  directus_email_smtp_secure   = var.sendinblue_smtp_secure

  directus_storage_scaleway_bucket   = scaleway_object_bucket.directus.name
  directus_storage_scaleway_endpoint = "s3.fr-par.scw.cloud"
  directus_storage_scaleway_key      = var.scaleway_default_access_key
  directus_storage_scaleway_region   = "fr-par"
  directus_storage_scaleway_secret   = var.scaleway_default_secret_key

  directus_cors_enabled = true
  directus_cors_origin  = true
}
