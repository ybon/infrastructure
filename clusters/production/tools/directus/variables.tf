variable "scaleway_access_key" {
  type = string
}

variable "scaleway_secret_key" {
  type = string
}

variable "scaleway_default_access_key" {
  type = string
}

variable "scaleway_default_secret_key" {
  type = string
}

variable "scaleway_default_project_id" {
  type = string
}

variable "directus_host" {
  type = string
}

variable "sendinblue_smtp_host" {
  type = string
}
variable "sendinblue_smtp_user" {
  type = string
}
variable "sendinblue_smtp_password" {
  type = string
}
variable "sendinblue_smtp_port" {
  type = string
}
variable "sendinblue_smtp_secure" {
  type = string
}