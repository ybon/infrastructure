locals {
  dns_zone           = "incubateur.anct.gouv.fr"
  dns_name           = "grist"
  domain             = "${local.dns_name}.${local.dns_zone}"
  grist_release_name = "grist"
  oauth_domain       = "chat.${local.dns_zone}"
}

resource "scaleway_domain_record" "grist" {
  provider = scaleway.default-project
  dns_zone = local.dns_zone
  name     = local.dns_name
  type     = "CNAME"
  data     = var.cluster_cname
  ttl      = 3600
}

resource "scaleway_object_bucket" "grist" {
  name   = "anct-grist-backups"
  acl    = "private"
  region = "fr-par"
}

module "grist_namespace" {
  source       = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version      = "0.2.0"
  max_cpu      = 4
  max_memory   = "11Gi"
  namespace    = "grist"
  project_name = "Grist"
  project_slug = "grist"
}

module "grist" {
  source     = "gitlab.com/vigigloo/tools-k8s/grist"
  version    = "1.0.2"
  namespace  = module.grist_namespace.namespace
  chart_name = local.grist_release_name
  values = [
    <<-EOT
    podAnnotations:
      monitoring-org-id: ${var.monitoring_org_id}
    envVars:
      APP_HOME_URL: https://${local.domain}
      GRIST_FORWARD_AUTH_HEADER: X-Forwarded-User
      GRIST_SINGLE_ORG: anct
      GRIST_FORWARD_AUTH_LOGOUT_PATH: _oauth/logout
      GRIST_ORG_IN_PATH: "true"
      GRIST_DEFAULT_EMAIL: "${var.default_email}"
      GRIST_SANDBOX_FLAVOR: gvisor
      GRIST_WIDGET_LIST_URL: https://infrastructure.pages.sit.incubateur.tech/grist-widgets/manifest.json
      GRIST_HIDE_UI_ELEMENTS: billing
EOT
  ]
  limits_cpu      = 1
  limits_memory   = "8Gi"
  requests_cpu    = "10m"
  requests_memory = "4Gi"

  backup_limits_cpu      = 1
  backup_limits_memory   = "1Gi"
  backup_requests_cpu    = "100m"
  backup_requests_memory = "512Mi"

  grist_persistence_size     = "10Gi"
  grist_backup_schedule      = "0 3 * * 1"
  grist_backup_s3_endpoint   = scaleway_object_bucket.grist.endpoint
  grist_backup_s3_region     = scaleway_object_bucket.grist.region
  grist_backup_s3_bucket     = scaleway_object_bucket.grist.name
  grist_backup_s3_access_key = var.scaleway_access_key
  grist_backup_s3_secret_key = var.scaleway_secret_key

  image_repository = "gristlabs/grist@sha256"
  image_tag        = "efee69b373a91dc70cad78d85cfaf22113703048639947daed47e72454c32c61"
}

resource "random_password" "auth_secret" {
  length  = 64
  special = false
}

module "auth" {
  source     = "gitlab.com/vigigloo/tools-k8s/traefikforwardauth"
  version    = "0.1.1"
  namespace  = module.grist_namespace.namespace
  chart_name = "auth"
  values = [
    <<-EOT
    ingress:
      annotations:
        haproxy.org/cors-enable: "true"
        haproxy.org/cors-allow-origin: "^https://.*(.anct.gouv.fr|incubateur.tech)$"
        haproxy.org/cors-allow-methods: "GET"
        haproxy.org/cors-allow-headers: "*"
    middleware:
      envVars:
        INSECURE_COOKIE: "true"
        DEFAULT_PROVIDER: "generic-oauth"
        PROVIDERS_GENERIC_OAUTH_AUTH_URL: "https://${local.oauth_domain}/oauth/authorize"
        PROVIDERS_GENERIC_OAUTH_TOKEN_URL: "https://${local.oauth_domain}/oauth/access_token"
        PROVIDERS_GENERIC_OAUTH_USER_URL: "https://${local.oauth_domain}/api/v4/users/me"
        PROVIDERS_GENERIC_OAUTH_CLIENT_ID: "${var.oauth_client_id}"
        PROVIDERS_GENERIC_OAUTH_CLIENT_SECRET: "${var.oauth_client_secret}"
        PROVIDERS_GENERIC_OAUTH_SCOPE: ""
        SECRET: ${random_password.auth_secret.result}
        LOGOUT_REDIRECT: "https://${local.domain}/signed-out"
EOT
  ]
  limits_cpu      = 1
  limits_memory   = "1Gi"
  requests_cpu    = "10m"
  requests_memory = "40Mi"
  ingress_host    = local.domain
  auth_paths = [
    "/auth/login",
    "/_oauth"
  ]
  auth_target_service = "http://${local.grist_release_name}"
}
