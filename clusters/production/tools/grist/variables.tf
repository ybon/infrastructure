variable "cluster_cname" {
  type = string
}

variable "scaleway_default_access_key" {
  type = string
}

variable "scaleway_default_secret_key" {
  type = string
}

variable "scaleway_default_project_id" {
  type = string
}

variable "scaleway_access_key" {
  type = string
}

variable "scaleway_secret_key" {
  type = string
}

variable "scaleway_project_id" {
  type = string
}

variable "oauth_client_id" {
  type = string
}

variable "oauth_client_secret" {
  type      = string
  sensitive = true
}

variable "default_email" {
  type = string
}

variable "monitoring_org_id" {
  type = string
}
