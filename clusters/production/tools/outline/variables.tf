variable "scaleway_default_access_key" {
  type = string
}

variable "scaleway_default_secret_key" {
  type = string
}

variable "scaleway_default_project_id" {
  type = string
}

variable "outline_postgresql" {
  type = string
}

variable "outline_clientId" {
  type = string
}

variable "outline_clientSecret" {
  type = string
}

variable "outline_email_smtp_host" {
  type = string
}
variable "outline_email_smtp_user" {
  type = string
}
variable "outline_email_smtp_password" {
  type = string
}

variable "cluster_cname" {
  type = string
}

variable "mattermost_host" {
  type = string
}
