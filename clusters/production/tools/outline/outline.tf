module "outline-namespace" {
  source       = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version      = "0.1.0"
  max_cpu      = 8
  max_memory   = "10Gi"
  namespace    = "outline"
  project_name = "Outline"
  project_slug = "outline"
}
resource "random_password" "secretKey" {
  length = 64
  lifecycle {
    ignore_changes = all
  }
}
resource "random_password" "utilsSecret" {
  length = 64
  lifecycle {
    ignore_changes = all
  }
}

resource "scaleway_object_bucket" "outline" {
  provider = scaleway.default-project
  name     = "incubateur-outline"
  acl      = "public-read"
  region   = "fr-par"
  lifecycle {
    ignore_changes = all
  }
}

module "outline" {
  source  = "gitlab.com/vigigloo/tools-k8s/outline"
  version = "0.1.0"

  chart_name    = "outline"
  chart_version = "0.1.0"
  namespace     = module.outline-namespace.namespace

  image_repository = "registry.gitlab.com/incubateur-territoires/incubateur/outline-mattermost-support"
  image_tag        = "v0.61.1"
  values = [
    file("${path.module}/outline.yaml"),
    <<-EOT
    postgresql:
      postgresqlUrl: ${var.outline_postgresql}
    EOT
  ]

  requests_cpu    = "10m"
  requests_memory = "400Mi"

  ingress_host        = "${scaleway_domain_record.outline.name}.${scaleway_domain_record.outline.dns_zone}"
  outline_secretKey   = random_password.secretKey.result
  outline_utilsSecret = random_password.utilsSecret.result
  outline_url         = "https://${scaleway_domain_record.outline.name}.${scaleway_domain_record.outline.dns_zone}"

  outline_storage_s3_bucket        = scaleway_object_bucket.outline.name
  outline_storage_s3_endpoint      = "https://${scaleway_object_bucket.outline.name}.s3.fr-par.scw.cloud"
  outline_storage_s3_region        = "fr-par"
  outline_storage_s3_uploadMaxSize = "26214400"
  outline_storage_s3_key           = var.scaleway_default_access_key
  outline_storage_s3_secret        = var.scaleway_default_secret_key

  outline_redis_redisUrl = "redis://${helm_release.redis.metadata[0].name}-master.${helm_release.redis.metadata[0].namespace}:6379"

  outline_thirdPartyAuth_openidConnect_displayName  = "Mattermost"
  outline_thirdPartyAuth_openidConnect_scopes       = "openid profile email"
  outline_thirdPartyAuth_openidConnect_tokenUri     = "https://${var.mattermost_host}/oauth/access_token"
  outline_thirdPartyAuth_openidConnect_authUri      = "https://${var.mattermost_host}/oauth/authorize"
  outline_thirdPartyAuth_openidConnect_userInfoUri  = "https://${var.mattermost_host}/api/v4/users/me"
  outline_thirdPartyAuth_openidConnect_clientId     = var.outline_clientId
  outline_thirdPartyAuth_openidConnect_clientSecret = var.outline_clientSecret

  outline_email_reply         = "outline@incubateur.anct.gouv.fr"
  outline_email_from          = "outline@incubateur.anct.gouv.fr"
  outline_email_smtp_host     = "https://smtp-relay.sendinblue.com"
  outline_email_smtp_user     = "incubateur@anct.gouv.fr"
  outline_email_smtp_password = "xsmtpsib-67689d7584426572ed7eb6f366aad7e6960605194ca0d44f55464274e27070fa-n4I5qQ8ZB7VJxDmL"

  outline_thirdPartyAuth_mattermost_tokenUri     = "https://${var.mattermost_host}/oauth/access_token"
  outline_thirdPartyAuth_mattermost_authorizeUri = "https://${var.mattermost_host}/oauth/authorize"
  outline_thirdPartyAuth_mattermost_profileUri   = "https://${var.mattermost_host}/api/v4/users/me"
  outline_thirdPartyAuth_mattermost_clientId     = var.outline_clientId
  outline_thirdPartyAuth_mattermost_clientSecret = var.outline_clientSecret
}
