resource "scaleway_domain_record" "outline" {
  provider = scaleway.default-project
  dns_zone = "incubateur.anct.gouv.fr"
  name     = "outline"
  type     = "CNAME"
  data     = var.cluster_cname
  ttl      = 3600
}
