resource "helm_release" "redis" {
  name       = "redis"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "redis"
  version    = "15.7.6"
  namespace  = module.outline-namespace.namespace

  set {
    name  = "auth.enabled"
    value = false
  }

  set {
    name  = "master.resources.limits.memory"
    value = "1024Mi"
  }

  set {
    name  = "master.resources.limits.cpu"
    value = "1"
  }

  set {
    name  = "master.resources.requests.memory"
    value = "100Mi"
  }

  set {
    name  = "master.resources.requests.cpu"
    value = "40m"
  }

  set {
    name  = "replica.resources.limits.memory"
    value = "1024Mi"
  }

  set {
    name  = "replica.resources.limits.cpu"
    value = "1"
  }

  set {
    name  = "replica.resources.requests.memory"
    value = "100Mi"
  }

  set {
    name  = "replica.resources.requests.cpu"
    value = "40m"
  }
}
