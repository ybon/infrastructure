resource "scaleway_domain_record" "matomo" {
  provider = scaleway.default-project
  dns_zone = "incubateur.anct.gouv.fr"
  name     = "matomo"
  type     = "CNAME"
  data     = var.cluster_cname
  ttl      = 3600
}
