resource "kubernetes_cron_job" "matomo_backup" {
  metadata {
    name      = "matomo-backup"
    namespace = module.namespace.namespace
  }

  spec {
    concurrency_policy            = "Replace"
    failed_jobs_history_limit     = 10
    schedule                      = "25 3 * * *"
    starting_deadline_seconds     = 10
    successful_jobs_history_limit = 10

    job_template {
      metadata {
        annotations = {}
        labels      = {}
      }

      spec {
        backoff_limit = 3

        template {
          metadata {
            annotations = {}
            labels      = {}
          }
          spec {
            container {
              name  = "ghost-backup"
              image = "registry.gitlab.com/incubateur-territoires/devops/infrastructure/docker-images/matomo-backup-alpine:latest"

              resources {
                limits = {
                  cpu    = "500m"
                  memory = "512Mi"
                }
              }

              env {
                name  = "NAME"
                value = "matomo_qonfucius_dev"
              }

              env {
                name  = "S3_REGION"
                value = local.s3_backup_region
              }

              env {
                name  = "S3_ENDPOINT_URL"
                value = "https://${local.s3_backup_bucket_name}.s3.${local.s3_backup_region}.scw.cloud"
              }

              env {
                name  = "S3_ACCESS_KEY"
                value = var.scaleway_access_key
              }

              env {
                name  = "S3_SECRET_KEY"
                value = var.scaleway_secret_key
              }

              env {
                name  = "MYSQL_HOST"
                value = module.mariadb.host
              }

              env {
                name  = "MYSQL_USER"
                value = "root"
              }

              env {
                name  = "MYSQL_PASSWORD"
                value = module.mariadb.root_password
              }

              volume_mount {
                name       = "data"
                mount_path = "/data"
                read_only  = true
              }
            }

            volume {
              name = "data"
              persistent_volume_claim {
                claim_name = "matomo"
              }
            }
          }
        }
      }
    }
  }
}
