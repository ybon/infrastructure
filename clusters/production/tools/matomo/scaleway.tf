locals {
  s3_backup_region      = "fr-par"
  s3_backup_bucket_name = "incubateur-matomo-backups-databases"
}
resource "scaleway_object_bucket" "matomo_backups" {
  name   = local.s3_backup_bucket_name
  region = local.s3_backup_region
}
