variable "cluster_cname" {
  type = string
}

variable "matomo_host" {
  type = string
}

variable "scaleway_default_access_key" {
  type = string
}

variable "scaleway_default_secret_key" {
  type = string
}

variable "scaleway_default_project_id" {
  type = string
}

variable "scaleway_access_key" {
  type = string
}

variable "scaleway_secret_key" {
  type = string
}

variable "scaleway_project_id" {
  type = string
}
