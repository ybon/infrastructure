resource "random_password" "matomo_db_password" {
  length  = 30
  special = false
  lifecycle {
    ignore_changes = all
  }
}

module "namespace" {
  source       = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version      = "0.3.1"
  max_cpu      = 2
  max_memory   = "3Gi"
  namespace    = "tools-matomo"
  project_name = "Matomo"
  project_slug = "matomo"
}

module "mariadb" {
  source     = "gitlab.com/vigigloo/tools-k8s/mariadb"
  version    = "0.1.1"
  chart_name = "mariadb"
  namespace  = module.namespace.namespace

  mariadb_auth_database = "matomo"
  mariadb_auth_username = "matomo"
  mariadb_auth_password = random_password.matomo_db_password.result
}

module "matomo" {
  source        = "gitlab.com/vigigloo/tools-k8s/matomo"
  version       = "1.0.0"
  chart_name    = "matomo"
  chart_version = "1.5.2"
  namespace     = module.namespace.namespace
  image_tag     = "4.11.0-fpm-alpine"
  values = [
    <<-EOT
  ingress:
    annotations:
      kubernetes.io/ingress.class: haproxy
      acme.cert-manager.io/http01-edit-in-place: "true"
      cert-manager.io/cluster-issuer: letsencrypt-prod
      haproxy.org/response-set-header: Referrer-Policy
    hosts:
      - ${var.matomo_host}
    tls:
      - secretName: matomo-tls
        hosts:
          - ${var.matomo_host}
    EOT
  ]

  limits_cpu      = "400m"
  limits_memory   = "1024Mi"
  requests_cpu    = "400m"
  requests_memory = "512Mi"

  matomo_database_host     = module.mariadb.host
  matomo_database_dbname   = "matomo"
  matomo_database_username = "matomo"
  matomo_database_password = random_password.matomo_db_password.result
}
