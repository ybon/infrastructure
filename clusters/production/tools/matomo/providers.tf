provider "scaleway" {
  region     = "fr-par"
  zone       = "fr-par-1"
  project_id = var.scaleway_project_id
  access_key = var.scaleway_access_key
  secret_key = var.scaleway_secret_key
}

provider "scaleway" {
  alias      = "default-project"
  region     = "fr-par"
  zone       = "fr-par-1"
  project_id = var.scaleway_default_project_id
  access_key = var.scaleway_default_access_key
  secret_key = var.scaleway_default_secret_key
}
