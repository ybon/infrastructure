module "forum" {
  source              = "./external-projects/forum"
  scaleway_access_key = var.scaleway_default_access_key
  scaleway_secret_key = var.scaleway_default_secret_key
  scaleway_project_id = var.scaleway_default_project_id
}

module "territoires-store" {
  source              = "./external-projects/territoires.store"
  scaleway_access_key = var.scaleway_default_access_key
  scaleway_secret_key = var.scaleway_default_secret_key
  scaleway_project_id = var.scaleway_default_project_id
  loadbalancer_ip     = scaleway_lb_ip.haproxy-ip.ip_address
}

module "dotations-locales" {
  source              = "./external-projects/dotations-locales"
  scaleway_access_key = var.scaleway_default_access_key
  scaleway_secret_key = var.scaleway_default_secret_key
  scaleway_project_id = var.scaleway_default_project_id
}
