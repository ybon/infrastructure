module "decidim" {
  source              = "./tools/decidim"
  scaleway_access_key = var.scaleway_access_key
  scaleway_secret_key = var.scaleway_secret_key
  decidim_host        = var.decidim_host
}

module "decidim_maires" {
  source                      = "./tools/decidim_maires"
  scaleway_access_key         = var.scaleway_access_key
  scaleway_secret_key         = var.scaleway_secret_key
  scaleway_project_id         = var.scaleway_project_id
  scaleway_default_access_key = var.scaleway_default_access_key
  scaleway_default_secret_key = var.scaleway_default_secret_key
  scaleway_default_project_id = var.scaleway_default_project_id
  decidim_host                = var.decidim_maires_host
  decidim_smtp_user           = var.decidim_maires_smtp_user
  decidim_smtp_password       = var.decidim_maires_smtp_password
  cluster_cname               = var.cluster_cname
  kubeconfig                  = scaleway_k8s_cluster.production.kubeconfig[0]
}

module "directus" {
  source                      = "./tools/directus"
  scaleway_access_key         = var.scaleway_access_key
  scaleway_secret_key         = var.scaleway_secret_key
  scaleway_default_access_key = var.scaleway_default_access_key
  scaleway_default_secret_key = var.scaleway_default_secret_key
  scaleway_default_project_id = var.scaleway_default_project_id
  directus_host               = var.directus_host
  sendinblue_smtp_host        = var.sendinblue_smtp_host
  sendinblue_smtp_user        = var.sendinblue_smtp_user
  sendinblue_smtp_password    = var.sendinblue_smtp_password
  sendinblue_smtp_port        = var.sendinblue_smtp_port
  sendinblue_smtp_secure      = var.sendinblue_smtp_secure
}

module "mattermost" {
  source                = "./tools/mattermost"
  scaleway_access_key   = var.scaleway_default_access_key
  scaleway_secret_key   = var.scaleway_default_secret_key
  scaleway_project_id   = var.scaleway_default_project_id
  mattermost_host       = var.mattermost_host
  mattermost_postgresql = var.mattermost_postgresql
  cluster_cname         = var.cluster_cname
}

module "outline" {
  source                      = "./tools/outline"
  scaleway_default_access_key = var.scaleway_default_access_key
  scaleway_default_secret_key = var.scaleway_default_secret_key
  scaleway_default_project_id = var.scaleway_default_project_id
  mattermost_host             = var.mattermost_host
  outline_postgresql          = var.outline_postgresql
  outline_clientId            = var.outline_clientId
  outline_clientSecret        = var.outline_clientSecret
  cluster_cname               = var.cluster_cname
  outline_email_smtp_host     = var.outline_email_smtp_host
  outline_email_smtp_password = var.outline_email_smtp_password
  outline_email_smtp_user     = var.outline_email_smtp_user
}

module "grist" {
  source              = "./tools/grist"
  scaleway_access_key = var.scaleway_access_key
  scaleway_secret_key = var.scaleway_secret_key
  scaleway_project_id = var.scaleway_project_id
  cluster_cname       = var.cluster_cname
  oauth_client_id     = var.grist_oauth_client_id
  oauth_client_secret = var.grist_oauth_client_secret
  default_email       = var.grist_default_email

  scaleway_default_access_key = var.scaleway_default_access_key
  scaleway_default_secret_key = var.scaleway_default_secret_key
  scaleway_default_project_id = var.scaleway_default_project_id

  monitoring_org_id = var.grist_monitoring_org_id
}

module "matomo" {
  source              = "./tools/matomo"
  matomo_host         = var.matomo_host
  scaleway_access_key = var.scaleway_access_key
  scaleway_secret_key = var.scaleway_secret_key
  scaleway_project_id = var.scaleway_project_id
  cluster_cname       = var.cluster_cname

  scaleway_default_access_key = var.scaleway_default_access_key
  scaleway_default_secret_key = var.scaleway_default_secret_key
  scaleway_default_project_id = var.scaleway_default_project_id

}
