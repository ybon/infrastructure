resource "kubernetes_namespace" "crunchydata-pgo" {
  metadata {
    name = "postgresql-operator"
  }
}

module "crunchydata-pgo" {
  source        = "gitlab.com/vigigloo/tools-k8s-crunchydata/pgo"
  version       = "0.0.3"
  namespace     = kubernetes_namespace.crunchydata-pgo.metadata[0].name
  chart_name    = "pgo"
  chart_version = "0.2.4"
}
