variable "cluster_cname" {
  type = string
}

variable "acme_notification_email" {
  type = string
}

variable "scaleway_project_id" {
  type = string
}

variable "scaleway_access_key" {
  type      = string
  sensitive = true
}

variable "scaleway_secret_key" {
  type      = string
  sensitive = true
}

variable "scaleway_default_project_id" {
  type = string
}

variable "scaleway_default_access_key" {
  type      = string
  sensitive = true
}

variable "scaleway_default_secret_key" {
  type      = string
  sensitive = true
}

variable "decidim_host" {
  type = string
}

variable "directus_host" {
  type = string
}

variable "matomo_host" {
  type = string
}

variable "mattermost_host" {
  type = string
}

variable "mattermost_postgresql" {
  type = string
}

variable "sendinblue_smtp_host" {
  type = string
}
variable "sendinblue_smtp_user" {
  type = string
}
variable "sendinblue_smtp_password" {
  type      = string
  sensitive = true
}
variable "sendinblue_smtp_port" {
  type = string
}
variable "sendinblue_smtp_secure" {
  type = string
}

variable "outline_postgresql" {
  type = string
}
variable "outline_clientId" {
  type = string
}
variable "outline_clientSecret" {
  type = string
}

variable "outline_email_smtp_host" {
  type = string
}
variable "outline_email_smtp_user" {
  type = string
}
variable "outline_email_smtp_password" {
  type      = string
  sensitive = true
}

variable "decidim_maires_host" {
  type = string
}
variable "decidim_maires_smtp_user" {
  type      = string
  sensitive = true
}
variable "decidim_maires_smtp_password" {
  type      = string
  sensitive = true
}

variable "grafana_host" {
  type = string
}

variable "grist_oauth_client_id" {
  type = string
}

variable "grist_oauth_client_secret" {
  type      = string
  sensitive = true
}

variable "grist_default_email" {
  type = string
}

variable "grist_monitoring_org_id" {
  type = string
}
