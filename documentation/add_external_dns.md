# Add external DNS record for a project deployed outside Kubernetes

## Create a dedicated Terraform module

### Create module from template
```sh
cd clusters/production/external-projects
cp -r template <your_project_name>
```

__/!\ No need to update `versions.tf` and `providers.tf` files__

### Update dns config
Inside the folder you just created:
- Change module name by the one of your choice, it will not affect anything else than its reference inside Terraform:
```
resource "scaleway_domain_record" "mymodule" {
```

- Add desired subdomain, type and IP in name property:
```
  name     = "my-subdomain"
  type     = "A"
  data     = "121.0.0.1"
```

## Update external projects config

At the end of `clusters/production/external-projects.tf` add the following declaration:

```
module <your_module_name> {
  source              = "<path_to_your_created_terraform_module>"
  scaleway_access_key = var.scaleway_default_access_key
  scaleway_secret_key = var.scaleway_default_secret_key
  scaleway_project_id = var.scaleway_default_project_id
}
```
